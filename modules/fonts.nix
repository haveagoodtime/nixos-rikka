{ pkgs, ... }: {
  #fonts.enableFontDir = true;
  #fonts.fontDir.enable = true;
  fonts.fonts = with pkgs; [
    symbola
    (nerdfonts.override { fonts = [ "Mononoki" ]; })
  ];
  fonts.fontconfig = {
    enable = true;
    defaultFonts = {
      sansSerif = [ "mononoki Nerd Font" ];
      monospace = [ "mononoki Nerd Font Mono" ];
    };
    localConf = builtins.readFile ./local.xml;
  };
}
