{ pkgs, ... }: {

  # Use xmonad 0.17
  nixpkgs.overlays = [
    (self: super: {
      haskellPackages = super.haskellPackages.override {
        overrides = hself: hsuper: {
          xmonad = hsuper.xmonad_0_17_0;
          xmonad-contrib = hsuper.xmonad-contrib_0_17_0;
          xmonad-extras = hsuper.xmonad-extras_0_17_0;
        };
      };
    })
  ];

  # Xorg
  services.xserver = {
    enable = true;
    dpi = 150;
    displayManager.startx.enable = true;
    libinput.enable = true;
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
    displayManager = { defaultSession = "none+xmonad"; };
  };
  services.picom.enable = true;
  environment.variables = {
    GDK_SCALE = "2";
    GDK_DPI_SCALE = "0.5";
    _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
  };
  hardware.video.hidpi.enable = true;
}
