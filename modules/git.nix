{ pkgs, ... }:

{
  home-manager.users.me.programs.git = {
    enable = true;
    userName = "Have a good time.";
    userEmail = "i@niconiconi.xyz";
    signing = {
      key = "i@niconiconi.xyz";
      signByDefault = true;
    };
    extraConfig = { credential.helper = "store"; };
  };
}
