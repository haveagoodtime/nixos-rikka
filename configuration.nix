# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./m1-support
      ./modules/emacs.nix
      ./modules/fonts.nix
      ./modules/xorg.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = false;
  #boot.kernelBuildIsCross = true;
  boot.kernelBuildIs16K = false;

  networking.hostName = "rikka-mac";
  networking.wireless = {
    interfaces = [ "wlp1s0f0" ]; 
    enable = true;  # Enables wireless support via wpa_supplicant.
  };
  networking.supplicant = {
    "wlp1s0f0" = {
        configFile.path = "/etc/wpa_supplicant.conf";
    };
  };
  networking.extraHosts = ''
    140.82.113.3 gist.github.com
    185.199.108.133 raw.githubusercontent.com
  '';

  networking.useDHCP = false;
  networking.interfaces.wlp1s0f0.useDHCP = true;


  # Set your time zone.
  time.timeZone = "Asia/Shanghai";


  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  users.users.me = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; 
  };

  environment.systemPackages = with pkgs; [
    neofetch
    alacritty
    kitty
    rofi
    git
    feh
    xmobar
    fd
    ripgrep
    trayer
    emacsGit
    gnumake
    flameshot
    firefox
    qemu
    clojure
    jdk
    yarn
    python3
    gcc
    babashka
  ];
  nixpkgs.overlays = [
    (self: super: {
      qemu = super.qemu.overrideAttrs (oldAttrs: rec {
        patches = oldAttrs.patches ++ [
          ./qemu/0001-hw-arm-virt-Add-a-control-for-the-the-highmem-PCIe-M.patch
          ./qemu/0002-hw-arm-virt-Add-a-control-for-the-the-highmem-redist.patch
          ./qemu/0003-hw-arm-virt-Honor-highmem-setting-when-computing-the.patch
          ./qemu/0004-hw-arm-virt-Use-the-PA-range-to-compute-the-memory-m.patch
          ./qemu/0005-hw-arm-virt-Disable-highmem-devices-that-don-t-fit-i.patch
          ./qemu/0006-hw-arm-virt-Drop-superfluous-checks-against-highmem.patch
          ./qemu/0006-linux-user-signal.c-define-__SIGRTMIN-MAX-for-non-GN.patch
          ./qemu/CVE-2021-20255.patch
        ];
      });
    })
  ];
  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
  users.groups.libvirtd.members = [ "root" "me" ];
  nixpkgs.config.allowUnfree = true; 
  services.openssh.enable = true;
  system.stateVersion = "22.05"; 
}

